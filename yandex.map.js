/**
 * Created by Nikolay Sutkovoy on 08.08.14.
 */

ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [50.41802, 30.607968],
        zoom: 17,
        controls: ['smallMapDefaultSet']
    });

// Создадим элемент управления "Пробки".
    var trafficControl = new ymaps.control.TrafficControl({ state: {
        // Отображаются пробки "Сейчас".
        providerKey: 'traffic#actual',
        // Начинаем сразу показывать пробки на карте.
        trafficShown: true
    }});
// Добавим контрол на карту.
    myMap.controls.add(trafficControl);
// Получим ссылку на провайдер пробок "Сейчас" и включим показ инфоточек.
    trafficControl.getProvider('traffic#actual').state.set('infoLayerShown', true);


// АГТ Значек главного офиса
    myPlacemark = new ymaps.Placemark(myMap.getCenter(), {

    });
// Конец значка

// Создание метки
    var balun = new ymaps.Placemark(
// Координаты метки
        [50.41802, 30.607968], {
            balloonContentHeader: '<strong>АГТ ПЛЮС</strong>',
            balloonContentBody: '<img src="/cache/widgetkit/gallery/58/sortirovchnaya-c6b3c6f064.jpg" width="150" height="200" alt="Офис АГТ ПЛЮС"><br><b>ул. Сортировочная, 2 оф.8</b> <br> (для навигатора ул. Сортировочная, 1)<br><a href="http://maps.yandex.ua/?um=AfYn3oViYAsngD210xFECwztSymWX0ac&ll=30.608034%2C50.417999&spn=0.005504%2C0.001957&z=18&l=map%2Ctrf&trfm=cur" target="_blank" <br> Пробки</a> ',
            balloonContentFooter: 'Телефон: (044) 501-09-24, (050) 355-00-84, <br> (050) 355-00-81'

        }, {
            preset: 'islands#redDotIcon'
        });
    myMap.geoObjects.add(balun);
    balun.balloon.open();


//Склад 3
// Создаем многоугольник, используя класс GeoObject.
    var sklad3 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417647, 30.605044],
                    [50.417818, 30.605668],
                    [50.41759, 30.605813],
                    [50.417429, 30.605189]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Склады \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad3_12.png',
        // Цвет обводки.
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.


    });


// Добавляем многоугольник на карту
    myMap.geoObjects.add(sklad3);

//Склад 2
// Создаем многоугольник, используя класс GeoObject.
    var sklad2 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417297, 30.605097],
                    [50.417463, 30.605797],
                    [50.417365, 30.605856],
                    [50.417199, 30.605159]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Склады \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad4_6.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(sklad2);


//Склад 1
// Создаем многоугольник, используя класс GeoObject.
    var sklad1 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.41707, 30.605261],
                    [50.417215, 30.605831],
                    [50.417231, 30.605826],
                    [50.417228, 30.605822],
                    [50.417244, 30.605874],
                    [50.417231, 30.605885],
                    [50.417233, 30.605923],
                    [50.417126, 30.605986],
                    [50.417043, 30.605664],
                    [50.417018, 30.60567],
                    [50.416996, 30.605545],
                    [50.417016, 30.605533],
                    [50.416961, 30.605328]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(sklad1);

//Вход на склад 3
// Создаем многоугольник, используя класс GeoObject.
    var vhod3 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417582, 30.605075],
                    [50.417541, 30.605037],
                    [50.417576, 30.60501]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Вход в цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        //fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(vhod3);

//Вход на склад 2
// Создаем многоугольник, используя класс GeoObject.
    var vhod2 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417432, 30.605332],
                    [50.417408, 30.60538],
                    [50.417394, 30.605321]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Вход в цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        //fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(vhod2);


//Вход на склад 2
// Создаем многоугольник, используя класс GeoObject.
    var vhod1 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.41753, 30.605713],
                    [50.417504, 30.605756],
                    [50.417494, 30.605697]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Вход в цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        //fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(vhod1);


    //Вход на склад 4
// Создаем многоугольник, используя класс GeoObject.
    var vhod4 = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417315, 30.605241],
                    [50.417339, 30.605187],
                    [50.417353, 30.605241]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Вход в цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        //fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(vhod4);

    //Вход на Цех
// Создаем многоугольник, используя класс GeoObject.
    var vhodceh = new ymaps.GeoObject({
        // Описываем геометрию геообъекта.
        geometry: {
            // Тип геометрии - "Многоугольник".
            type: "Polygon",
            // Указываем координаты вершин многоугольника.
            coordinates: [
                // Координаты вершин внешнего контура.
                [
                    [50.417175, 30.605666],
                    [50.417199, 30.605607],
                    [50.417213, 30.605677]
                ]
            ],
            // Задаем правило заливки внутренних контуров по алгоритму "nonZero".
            fillRule: "nonZero"
        },
// Описываем свойства геообъекта.
        properties: {
            // Содержимое балуна.
            balloonContent: "Вход в цех \"АГТ ПЛЮС\""
        }
    }, {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#F01515',
        //fillImageHref: 'http://agtplus.ua/images/yandex-map/sklad5_2.png',
        // Цвет обводки.
        //strokeColor: '#0000FF',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.7,
        // Ширина обводки.
        strokeWidth: 0
        // Стиль обводки.
    });

// Добавляем многоугольник на карту.
    myMap.geoObjects.add(vhodceh);

// КП с Клеманской

    var KP1 = new ymaps.Placemark([50.415535, 30.603112], {}, {
        iconLayout: 'default#image',
        iconImageHref: 'http://agtplus.ua/images/yandex-map/kp1.png',
        iconImageSize: [20, 15],
        iconImageOffset: [0, 0]
    });

    var KP2 = new ymaps.Placemark([50.417413, 30.604299], {}, {
        iconLayout: 'default#image',
        iconImageHref: 'http://agtplus.ua/images/yandex-map/kp1.png',
        iconImageSize: [20, 15],
        iconImageOffset: [0, 0]
    });

    var KP3 = new ymaps.Placemark([50.418074, 30.606755], {}, {
        iconLayout: 'default#image',
        iconImageHref: 'http://agtplus.ua/images/yandex-map/kp1.png',
        iconImageSize: [20, 15]
        //iconImageOffset: [1, 15]
    });

    myMap.geoObjects.add(KP1);
    myMap.geoObjects.add(KP2);
    myMap.geoObjects.add(KP3);


//Стрелки заезда 1

// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.419073, 30.606758],
            [50.418479, 30.607083]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.418168, 30.606637],
            [50.417958, 30.605759]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            //strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.417874, 30.605539],
            [50.417667, 30.604855]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            //strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

//Стрелки заезда 2

    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.419124, 30.607769],
            [50.418242, 30.608284]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });


    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.417648, 30.608637],
            [50.41704, 30.608996]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.416859, 30.608798],
            [50.416365, 30.606781]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.416138, 30.605899],
            [50.415562, 30.603572]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });


    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.415814, 30.60309],
            [50.416719, 30.602565]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });


    myMap.geoObjects.add(myPlacemark);
// Пользовательские модули не дописываются в неймспейс ymaps.
// Поэтому доступ к ним мы можем получить асинхронно через метод ymaps.modules.require.
    ymaps.modules.require(['geoObject.Arrow'], function (Arrow) {
        var arrow = new Arrow([
            [50.416893, 30.602621],
            [50.417291, 30.604333]
        ], null, {
            geodesic: true,
            strokeWidth: 5,
            strokeColor: '#E80C0C',
            opacity: 0.5
            // strokeStyle: 'shortdash'
        });
        myMap.geoObjects.add(arrow);
    });

});


/*
 * Класс, позволяющий создавать стрелку на карте.
 * Является хелпером к созданию полилинии, у которой задан специальный оверлей.
 * При использовании модулей в реальном проекте рекомендуем размещать их в отдельных файлах.
 */
ymaps.modules.define("geoObject.Arrow", [
    'Polyline',
    'overlay.Arrow',
    'util.extend'
], function (provide, Polyline, ArrowOverlay, extend) {
    /**
     * @param {Number[][] | Object | ILineStringGeometry} geometry Геометрия ломаной.
     * @param {Object} properties Данные ломаной.
     * @param {Object} options Опции ломаной.
     * Поддерживается тот же набор опций, что и в классе ymaps.Polyline.
     * @param {Number} [options.arrowAngle=20] Угол в градусах между основной линией и линиями стрелки.
     * @param {Number} [options.arrowMinLength=3] Минимальная длина стрелки. Если длина стрелки меньше минимального значения, стрелка не рисуется.
     * @param {Number} [options.arrowMaxLength=20] Максимальная длина стрелки.
     */
    var Arrow = function (geometry, properties, options) {
        return new Polyline(geometry, properties, extend({}, options, {
            lineStringOverlay: ArrowOverlay
        }));
    };
    provide(Arrow);
});

/*
 * Класс, реализующий интерфейс IOverlay.
 * Получает на вход пиксельную геометрию линии и добавляет стрелку на конце линии.
 */
ymaps.modules.define("overlay.Arrow", [
    'overlay.Polygon',
    'util.extend',
    'event.Manager',
    'option.Manager',
    'Event',
    'geometry.pixel.Polygon'
], function (provide, PolygonOverlay, extend, EventManager, OptionManager, Event, PolygonGeometry) {
    var domEvents = [
            'click',
            'contextmenu',
            'dblclick',
            'mousedown',
            'mouseenter',
            'mouseleave',
            'mousemove',
            'mouseup',
            'multitouchend',
            'multitouchmove',
            'multitouchstart',
            'wheel'
        ],

        /**
         * @param {geometry.pixel.Polyline} pixelGeometry Пиксельная геометрия линии.
         * @param {Object} data Данные оверлея.
         * @param {Object} options Опции оверлея.
         */
            ArrowOverlay = function (pixelGeometry, data, options) {
            // Поля .events и .options обязательные для IOverlay.
            this.events = new EventManager();
            this.options = new OptionManager(options);
            this._map = null;
            this._data = data;
            this._geometry = pixelGeometry;
            this._overlay = null;
        };

    ArrowOverlay.prototype = extend(ArrowOverlay.prototype, {
        // Реализовываем все методы и события, которые требует интерфейс IOverlay.
        getData: function () {
            return this._data;
        },

        setData: function (data) {
            if (this._data != data) {
                var oldData = this._data;
                this._data = data;
                this.events.fire('datachange', {
                    oldData: oldData,
                    newData: data
                });
            }
        },

        getMap: function () {
            return this._map;
        },

        setMap: function (map) {
            if (this._map != map) {
                var oldMap = this._map;
                if (!map) {
                    this._onRemoveFromMap();
                }
                this._map = map;
                if (map) {
                    this._onAddToMap();
                }
                this.events.fire('mapchange', {
                    oldMap: oldMap,
                    newMap: map
                });
            }
        },

        setGeometry: function (geometry) {
            if (this._geometry != geometry) {
                var oldGeometry = geometry;
                this._geometry = geometry;
                if (this.getMap() && geometry) {
                    this._rebuild();
                }
                this.events.fire('geometrychange', {
                    oldGeometry: oldGeometry,
                    newGeometry: geometry
                });
            }
        },

        getGeometry: function () {
            return this._geometry;
        },

        getShape: function () {
            return null;
        },

        isEmpty: function () {
            return false;
        },

        _rebuild: function () {
            this._onRemoveFromMap();
            this._onAddToMap();
        },

        _onAddToMap: function () {
            // Военная хитрость - чтобы в прозрачной ломаной хорошо отрисовывались самопересечения,
            // мы рисуем вместо линии многоугольник.
            // Каждый контур многоугольника будет отвечать за часть линии.
            this._overlay = new PolygonOverlay(new PolygonGeometry(this._createArrowContours()));
            this._startOverlayListening();
            // Эта строчка свяжет два менеджера опций.
            // Опции, заданные в родительском менеджере,
            // будут распространяться и на дочерний.
            this._overlay.options.setParent(this.options);
            this._overlay.setMap(this.getMap());
        },

        _onRemoveFromMap: function () {
            this._overlay.setMap(null);
            this._overlay.options.setParent(null);
            this._stopOverlayListening();
        },

        _startOverlayListening: function () {
            this._overlay.events.add(domEvents, this._onDomEvent, this);
        },

        _stopOverlayListening: function () {
            this._overlay.events.remove(domEvents, this._onDomEvent, this);
        },

        _onDomEvent: function (e) {
            // Мы слушаем события от дочернего служебного оверлея
            // и прокидываем их на внешнем классе.
            // Это делается для того, чтобы в событии было корректно определено
            // поле target.
            this.events.fire(e.get('type'), new Event({
                target: this
                // Свяжем исходное событие с текущим, чтобы все поля данных дочернего события
                // были доступны в производном событии.
            }, e));
        },

        _createArrowContours: function () {
            var contours = [],
                mainLineCoordinates = this.getGeometry().getCoordinates(),
                arrowLength = calculateArrowLength(
                    mainLineCoordinates,
                    this.options.get('arrowMinLength', 3),
                    this.options.get('arrowMaxLength', 20)
                );
            contours.push(getContourFromLineCoordinates(mainLineCoordinates));
            // Будем рисовать стрелку только если длина линии не меньше длины стрелки.
            if (arrowLength > 0) {
                // Создадим еще 2 контура для стрелочек.
                var lastTwoCoordinates = [
                        mainLineCoordinates[mainLineCoordinates.length - 2],
                        mainLineCoordinates[mainLineCoordinates.length - 1]
                    ],
                // Для удобства расчетов повернем стрелку так, чтобы она была направлена вдоль оси y,
                // а потом развернем результаты обратно.
                    rotationAngle = getRotationAngle(lastTwoCoordinates[0], lastTwoCoordinates[1]),
                    rotatedCoordinates = rotate(lastTwoCoordinates, rotationAngle),

                    arrowAngle = this.options.get('arrowAngle', 20) / 180 * Math.PI,
                    arrowBeginningCoordinates = getArrowsBeginningCoordinates(
                        rotatedCoordinates,
                        arrowLength,
                        arrowAngle
                    ),
                    firstArrowCoordinates = rotate([
                        arrowBeginningCoordinates[0],
                        rotatedCoordinates[1]
                    ], -rotationAngle),
                    secondArrowCoordinates = rotate([
                        arrowBeginningCoordinates[1],
                        rotatedCoordinates[1]
                    ], -rotationAngle);

                contours.push(getContourFromLineCoordinates(firstArrowCoordinates));
                contours.push(getContourFromLineCoordinates(secondArrowCoordinates));
            }
            return contours;
        }
    });

    function getArrowsBeginningCoordinates(coordinates, arrowLength, arrowAngle) {
        var p1 = coordinates[0],
            p2 = coordinates[1],
            dx = arrowLength * Math.sin(arrowAngle),
            y = p2[1] - arrowLength * Math.cos(arrowAngle);
        return [
            [p1[0] - dx, y],
            [p1[0] + dx, y]
        ];
    }

    function rotate(coordinates, angle) {
        var rotatedCoordinates = [];
        for (var i = 0, l = coordinates.length, x, y; i < l; i++) {
            x = coordinates[i][0];
            y = coordinates[i][1];
            rotatedCoordinates.push([
                x * Math.cos(angle) - y * Math.sin(angle),
                x * Math.sin(angle) + y * Math.cos(angle)
            ]);
        }
        return rotatedCoordinates;
    }

    function getRotationAngle(p1, p2) {
        return Math.PI / 2 - Math.atan2(p2[1] - p1[1], p2[0] - p1[0]);
    }

    function getContourFromLineCoordinates(coords) {
        var contour = coords.slice();
        for (var i = coords.length - 2; i > -1; i--) {
            contour.push(coords[i]);
        }
        return contour;
    }

    function calculateArrowLength(coords, minLength, maxLength) {
        var linePixelLength = 0;
        for (var i = 1, l = coords.length; i < l; i++) {
            linePixelLength += getVectorLength(
                coords[i][0] - coords[i - 1][0],
                coords[i][1] - coords[i - 1][1]
            );
            if (linePixelLength / 3 > maxLength) {
                return maxLength;
            }
        }
        var finalArrowLength = linePixelLength / 3;
        return finalArrowLength < minLength ? 0 : finalArrowLength;
    }

    function getVectorLength(x, y) {
        return Math.sqrt(x * x + y * y);
    }

    provide(ArrowOverlay);
});


