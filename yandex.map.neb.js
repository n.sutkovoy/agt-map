/**
 * Created by Nikolay Sutkovoy on 08.08.14.
 */

ymaps.ready(function () {
    var myMapN = new ymaps.Map('map-nebel', {
        center: [50.415435, 29.612083],
        zoom: 17,
        controls: ['smallMapDefaultSet']
    });

// Создадим элемент управления "Пробки".
    var trafficControl = new ymaps.control.TrafficControl({ state: {
        // Отображаются пробки "Сейчас".
        providerKey: 'traffic#actual',
        // Начинаем сразу показывать пробки на карте.
        trafficShown: true
    }});
// Добавим контрол на карту.
    myMapN.controls.add(trafficControl);
// Получим ссылку на провайдер пробок "Сейчас" и включим показ инфоточек.
    trafficControl.getProvider('traffic#actual').state.set('infoLayerShown', true);


// АГТ Значек главного офиса
    myPlacemark = new ymaps.Placemark(myMapN.getCenter(), {

    });
// Конец значка

// Создание метки
    var balun = new ymaps.Placemark(
// Координаты метки
        [50.415435, 29.612083], {
            balloonContentHeader: '<strong>АГТ ПЛЮС</strong>',
            balloonContentBody: '<img src="http://agtplus.ua/images/news/nebelica/nebelica.jpg" width="180" height="194" alt="Склад АГТ ПЛЮС"><br><b>Киевская обл., Макаровский р-н.,<br> село Небелица, ул. Ленина 1Б</b>',
            balloonContentFooter: 'Телефон: (045) 78-42-847'

        }, {
            preset: 'islands#redDotIcon'
        });
    myMapN.geoObjects.add(balun);
    balun.balloon.open();
});
